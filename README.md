# locksmith

> a locksmith simulator for MB.

## Running Instructions:
> docker run -p 5000:5000 registry.gitlab.com/nuriu/locksmith/backend:latest  
> docker run -p 4200:80 registry.gitlab.com/nuriu/locksmith/frontend:latest

## More About Me:
https://github.com/nuriu

https://hackerrank.com/nuriu

https://linkedin.com/in/nuriuzunoglu