import { Component, OnInit } from '@angular/core';
import DoorStates from 'src/models/doorStates';
import { CarService } from 'src/services/car.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  activeSide: 'left' | 'right' | null = null;

  activeSideDoorStates: {
    front: string;
    rear: string;
  } | null = null;

  private doorStates!: DoorStates;

  constructor(public carService: CarService) {}

  ngOnInit(): void {
    let stateIndex = window.location.href.indexOf('state');

    if (stateIndex != -1) {
      if (
        window.location.href.substring(stateIndex + 6) !== this.carService.state
      ) {
        console.error('state mismatch');
      }
    }

    let codeIndex = window.location.href.indexOf('code');
    if (codeIndex != -1) {
      let code = window.location.href.substring(codeIndex + 5, stateIndex - 1);
      this.carService.retrieveToken(code);
    }
  }

  lockDoors(): void {
    this.carService.lockAllDoors().subscribe((res) => {});
  }

  unlockDoors(): void {
    this.carService.unlockAllDoors().subscribe((res) => {});
  }

  getStatesOfDoors(): void {
    this.carService.getStatesOfDoors().subscribe((res) => {
      this.doorStates = res as DoorStates;

      if (!this.activeSide) {
        this.changeSide('right');
      } else {
        this.changeSide(this.activeSide);
      }

      setTimeout(() => {
        this.getStatesOfDoors();
      }, 2000);
    });
  }

  changeSide(side: 'left' | 'right'): void {
    this.activeSide = side;

    this.activeSideDoorStates = { front: '', rear: '' };

    if (this.activeSide === 'left') {
      this.activeSideDoorStates.front =
        this.doorStates.doorlockstatusfrontleft.value;
      this.activeSideDoorStates.rear =
        this.doorStates.doorlockstatusrearleft.value;
    } else if (this.activeSide === 'right') {
      this.activeSideDoorStates.front =
        this.doorStates.doorlockstatusfrontright.value;
      this.activeSideDoorStates.rear =
        this.doorStates.doorlockstatusrearright.value;
    }
  }

  authorize(): void {
    window.location.href = this.carService.authUrl;
  }
}
