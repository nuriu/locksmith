import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Vehicle from 'src/models/vehicle';

@Injectable({
  providedIn: 'root',
})
export class CarService {
  private clientId: string = '5da6ded1-a464-4129-97c2-5e7aa3f84053';
  private scopes: string[] = [
    'mb:user:pool:reader',
    'mb:vehicle:status:general',
    'offline_access',
  ];

  private redirectUri: string = 'http://localhost:4200';
  private apiUrl: string = 'http://localhost:5000';

  private token!: string;
  private vehicle!: Vehicle;

  state: string = 'locksmith-simulator';
  isReady: boolean = false;

  constructor(private _http: HttpClient) {}

  getStatesOfDoors(): Observable<object> {
    return this._http.get(
      this.apiUrl + '/locksmith/DoorStates?vehicleId=' + this.vehicle.id,
      {
        headers: {
          MB_TOKEN: this.token,
        },
      }
    );
  }

  lockAllDoors(): Observable<object> {
    return this._http.get(
      this.apiUrl + '/locksmith/LockDoors?vehicleId=' + this.vehicle.id,
      {
        headers: {
          MB_TOKEN: this.token,
        },
      }
    );
  }

  unlockAllDoors(): Observable<object> {
    return this._http.get(
      this.apiUrl + '/locksmith/UnlockDoors?vehicleId=' + this.vehicle.id,
      {
        headers: {
          MB_TOKEN: this.token,
        },
      }
    );
  }

  get authUrl(): string {
    return `https://id.mercedes-benz.com/as/authorization.oauth2?response_type=code&client_id=${
      this.clientId
    }&redirect_uri=${this.redirectUri}&scope=${this.scopes.join(' ')}&state=${
      this.state
    }`;
  }

  retrieveToken(code: string): void {
    this._http
      .get(this.apiUrl + `/locksmith/ExchangeCode?code=${code}`)
      .subscribe((res: any) => {
        this.token = res['access_token'];
        this.loadVehicles();
      });
  }

  private loadVehicles(): void {
    this._http
      .get(this.apiUrl + '/locksmith/Vehicles', {
        headers: {
          MB_TOKEN: this.token,
        },
      })
      .subscribe((res: any) => {
        this.vehicle = res[0] as Vehicle;
        this.isReady = true;
      });
  }
}
