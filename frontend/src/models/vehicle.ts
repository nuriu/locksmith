export default class Vehicle {
  id!: string;
  licenseplate!: string;
  finorvin!: string;
  salesdesignation!: string;
  nickname!: string;
  modelyear!: string;
  colorname!: string;
  fueltype!: string;
  powerhp!: string;
  powerkw!: string;
  numberofdoors!: string;
  numberofseats!: string;
}
