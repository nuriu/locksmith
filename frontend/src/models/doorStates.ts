export class DoorState {
  value!: string;
  retrievalstatus!: string;
  timestamp!: number;
}

export default class DoorStates {
  doorstatusfrontleft!: DoorState;
  doorlockstatusfrontleft!: DoorState;
  doorstatusfrontright!: DoorState;
  doorlockstatusfrontright!: DoorState;
  doorstatusrearright!: DoorState;
  doorlockstatusrearright!: DoorState;
  doorstatusrearleft!: DoorState;
  doorlockstatusrearleft!: DoorState;
  doorlockstatusdecklid!: DoorState;
  doorlockstatusgas!: DoorState;
  doorlockstatusvehicle!: DoorState;
}
