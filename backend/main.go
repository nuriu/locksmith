package main

import (
	"backend/locksmith"
	"log"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	app := locksmith.NewApp()
	err = app.Serve()

	if err != nil {
		log.Fatal("Error", err)
	}
}
