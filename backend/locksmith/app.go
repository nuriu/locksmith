package locksmith

import (
	"backend/locksmith/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type App struct {
	handlers map[string]http.HandlerFunc
}

func NewApp() App {
	app := App{
		handlers: make(map[string]http.HandlerFunc),
	}

	healthcheck := disableCors(app.healthcheck)
	exchangeCode := disableCors(app.exchangeCode)
	getVehicles := disableCors(app.getVehicles)
	// getVehicle := disableCors(app.getVehicle)
	getDoorStates := disableCors(app.getDoorStates)
	lockDoors := disableCors(app.lockDoors)
	unlockDoors := disableCors(app.unlockDoors)

	app.handlers["/healthcheck"] = healthcheck
	app.handlers["/locksmith/ExchangeCode"] = exchangeCode
	app.handlers["/locksmith/Vehicles"] = getVehicles
	// app.handlers["/locksmith/Vehicle"] = getVehicle
	app.handlers["/locksmith/DoorStates"] = getDoorStates
	app.handlers["/locksmith/LockDoors"] = lockDoors
	app.handlers["/locksmith/UnlockDoors"] = unlockDoors

	return app
}

func (a *App) Serve() error {
	for path, handler := range a.handlers {
		http.Handle(path, handler)
	}
	log.Println("Web server is available on port 5000")
	return http.ListenAndServe("0.0.0.0:5000", nil)
}

func (a *App) healthcheck(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Healthy!"))
}

func (a *App) exchangeCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")
	if code == "" {
		sendErr(w, http.StatusInternalServerError, "Wrong code!")
	}
	fmt.Println("exchangeCode - code = " + code)

	body, statusCode, err := services.ExchangeCodeForToken(code)
	if err != nil {
		sendErr(w, statusCode, err.Error())
	} else {
		w.Write(body)
	}
}

func (a *App) getVehicles(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("MB_TOKEN")
	if token != "" {
		body, statusCode, err := services.GetVehicles(token)
		if err != nil {
			sendErr(w, statusCode, err.Error())
		} else {
			w.Write(body)
		}
	}
}

func (a *App) getDoorStates(w http.ResponseWriter, r *http.Request) {
	vehicleId := r.URL.Query().Get("vehicleId")
	if vehicleId == "" {
		sendErr(w, http.StatusBadRequest, "Wrong vehicleId!")
	}

	fmt.Println("getDoorStates - vehicleId = " + vehicleId)

	token := r.Header.Get("MB_TOKEN")
	if token != "" {
		body, statusCode, err := services.GetStatesOfDoors(token, vehicleId)
		if err != nil {
			sendErr(w, statusCode, err.Error())
		} else {
			w.Write(body)
		}
	}
}

func (a *App) lockDoors(w http.ResponseWriter, r *http.Request) {
	vehicleId := r.URL.Query().Get("vehicleId")
	if vehicleId == "" {
		sendErr(w, http.StatusBadRequest, "Wrong vehicleId!")
	}

	token := r.Header.Get("MB_TOKEN")
	if token != "" {
		body, statusCode, err := services.LockAllDoors(token, vehicleId)
		if err != nil {
			sendErr(w, statusCode, err.Error())
		} else {
			w.Write(body)
		}
	}
}

func (a *App) unlockDoors(w http.ResponseWriter, r *http.Request) {
	vehicleId := r.URL.Query().Get("vehicleId")
	if vehicleId == "" {
		sendErr(w, http.StatusBadRequest, "Wrong vehicleId!")
	}

	token := r.Header.Get("MB_TOKEN")
	if token != "" {
		body, statusCode, err := services.UnlockAllDoors(token, vehicleId)
		if err != nil {
			sendErr(w, statusCode, err.Error())
		} else {
			w.Write(body)
		}
	}
}

func sendErr(w http.ResponseWriter, code int, message string) {
	resp, _ := json.Marshal(map[string]string{"error": message})
	http.Error(w, string(resp), code)
}

func disableCors(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		h(w, r)
	}
}
