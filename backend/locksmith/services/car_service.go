package services

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
)

func GetVehicles(token string) ([]byte, int, error) {
	req, err := http.NewRequest(http.MethodGet, os.Getenv("CarApiUri")+"/vehicles", nil)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, http.StatusFailedDependency, errors.New("UNSUCCESSFUL API REQUEST")
	} else {
		return body, resp.StatusCode, nil
	}
}

func GetStatesOfDoors(token string, vehicleId string) ([]byte, int, error) {
	req, err := http.NewRequest(http.MethodGet, os.Getenv("CarApiUri")+"/vehicles/"+vehicleId+"/doors", nil)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, http.StatusFailedDependency, errors.New("UNSUCCESSFUL API REQUEST")
	} else {
		return body, resp.StatusCode, nil
	}
}

func LockAllDoors(token string, vehicleId string) ([]byte, int, error) {
	var jsonData = []byte(`{
		"command": "LOCK"
	}`)

	req, err := http.NewRequest(http.MethodPost, os.Getenv("CarApiUri")+"/vehicles/"+vehicleId+"/doors", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, http.StatusFailedDependency, errors.New("UNSUCCESSFUL API REQUEST")
	} else {
		return body, resp.StatusCode, nil
	}
}

func UnlockAllDoors(token string, vehicleId string) ([]byte, int, error) {
	var jsonData = []byte(`{
		"command": "UNLOCK"
	}`)

	req, err := http.NewRequest(http.MethodPost, os.Getenv("CarApiUri")+"/vehicles/"+vehicleId+"/doors", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, http.StatusFailedDependency, errors.New("UNSUCCESSFUL API REQUEST")
	} else {
		return body, resp.StatusCode, nil
	}
}
