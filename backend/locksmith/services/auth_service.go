package services

import (
	"encoding/base64"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

func ExchangeCodeForToken(code string) ([]byte, int, error) {
	data := url.Values{}
	data.Set("code", code)
	data.Set("grant_type", "authorization_code")
	data.Set("redirect_uri", os.Getenv("RedirectUri"))

	req, err := http.NewRequest(http.MethodPost, os.Getenv("TokenUri"), strings.NewReader(data.Encode()))
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	token := base64.StdEncoding.EncodeToString([]byte(os.Getenv("ClientId") + ":" + os.Getenv("ClientSecret")))
	req.Header.Set("Authorization", "Basic "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, http.StatusFailedDependency, errors.New("UNSUCCESSFUL API REQUEST")
	} else {
		return body, resp.StatusCode, nil
	}
}
